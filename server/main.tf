terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "3.23.0"
    }
  }
}

# Configure the Microsoft Azure Provider
provider "azurerm" {
  features {}
}
variable "vpc_cidr_block" {}
variable "subnet_cidr_block" {}

# Create a resource group
resource "azurerm_resource_group" "tf" {
  name     = "resources-gp"
  location = "Brazil South"
}
resource "tls_private_key" "key" {
  algorithm = "RSA"
  rsa_bits  = 4096
}
resource "local_file" "linuxkey" {
  content  = tls_private_key.key.private_key_pem
  filename = "linuxkey.pem"
}
# Create a virtual network within the resource group
resource "azurerm_virtual_network" "vpc" {
  name                = "network"
  resource_group_name = azurerm_resource_group.tf.name
  location            = azurerm_resource_group.tf.location
  address_space       = var.vpc_cidr_block

}

#Create a subnet  within the resource group
resource "azurerm_subnet" "az_subnet" {
  name                 = "net-subnet"
  resource_group_name  = azurerm_resource_group.tf.name
  virtual_network_name = azurerm_virtual_network.vpc.name
  address_prefixes     = var.subnet_cidr_block
}

resource "azurerm_route_table" "rtb" {
  name                          = "net-route-table"
  location                      = azurerm_resource_group.tf.location
  resource_group_name           = azurerm_resource_group.tf.name
  disable_bgp_route_propagation = false

  route {
    name           = "route1"
    address_prefix = "0.0.0.0/0"
    next_hop_type  = "Internet"
  }
}

resource "azurerm_subnet_route_table_association" "rtb-aasociate" {
  subnet_id      = azurerm_subnet.az_subnet.id
  route_table_id = azurerm_route_table.rtb.id
}
resource "azurerm_network_security_group" "tf-security" {
  name                = "acceptanceTestSecurityGroup1"
  location            = azurerm_resource_group.tf.location
  resource_group_name = azurerm_resource_group.tf.name
}

resource "azurerm_network_security_rule" "Inbound-security" {
  name                        = "rule1"
  priority                    = 100
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "*"
  source_port_range           = "*"
  destination_port_range      = "22"
  source_address_prefix       = "20.187.94.10"
  destination_address_prefix  = "*"
  resource_group_name         = azurerm_resource_group.tf.name
  network_security_group_name = azurerm_network_security_group.tf-security.name
}
resource "azurerm_network_security_rule" "Inbound-ssecurity" {
  name                        = "rules1"
  priority                    = 130
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "*"
  source_port_range           = "*"
  destination_port_range      = "22"
  source_address_prefix       = azurerm_linux_virtual_machine.redirector.public_ip_address
  destination_address_prefix  = "*"
  resource_group_name         = azurerm_resource_group.tf.name
  network_security_group_name = azurerm_network_security_group.tf-security.name
}
# data "azurerm_virtual_machine" "redirector" {
#   name                = "redirector"
#   resource_group_name = "resources-gp3"
# }


resource "azurerm_network_security_rule" "Inbound-security2" {
  name                        = "rule2"
  priority                    = 200
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "*"
  source_port_range           = "*"
  destination_port_range      = "443"
  source_address_prefix       = azurerm_linux_virtual_machine.redirector.public_ip_address
  destination_address_prefix  = "*"
  resource_group_name         = azurerm_resource_group.tf.name
  network_security_group_name = azurerm_network_security_group.tf-security.name
}

resource "azurerm_network_security_rule" "outbound-security" {
  name                        = "rule3"
  priority                    = 200
  direction                   = "Outbound"
  access                      = "Allow"
  protocol                    = "*"
  source_port_range           = "*"
  destination_port_range      = "*"
  source_address_prefix       = "*"
  destination_address_prefix  = "*"
  resource_group_name         = azurerm_resource_group.tf.name
  network_security_group_name = azurerm_network_security_group.tf-security.name
}

resource "azurerm_public_ip" "tp-pub-ip" {
  name                = "server-public-ip"
  resource_group_name = azurerm_resource_group.tf.name
  location            = azurerm_resource_group.tf.location
  allocation_method   = "Dynamic"
}

resource "azurerm_subnet_network_security_group_association" "sub_nsg" {
  subnet_id                 = azurerm_subnet.az_subnet.id
  network_security_group_id = azurerm_network_security_group.tf-security.id
}

resource "azurerm_network_interface" "nic" {
  name                = "nic2"
  location            = azurerm_resource_group.tf.location
  resource_group_name = azurerm_resource_group.tf.name

  ip_configuration {
    name                          = "nic-config"
    subnet_id                     = azurerm_subnet.az_subnet.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.tp-pub-ip.id
  }
}

resource "azurerm_network_interface_security_group_association" "example" {
  network_interface_id      = azurerm_network_interface.nic.id
  network_security_group_id = azurerm_network_security_group.tf-security.id
}

resource "azurerm_linux_virtual_machine" "tf-linux" {
  name                            = "Merlin-c2"
  resource_group_name             = azurerm_resource_group.tf.name
  location                        = azurerm_resource_group.tf.location
  size                            = "Standard_D2s_v3"
  admin_username                  = "muha"
  disable_password_authentication = true
  network_interface_ids = [
    azurerm_network_interface.nic.id,
  ]

  admin_ssh_key {
    username   = "muha"
    public_key = tls_private_key.key.public_key_openssh
  }

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "StandardSSD_LRS"
  }

  source_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "18.04-LTS"
    version   = "latest"
  }

  connection {
    type        = "ssh"
    user        = "muha"
    host        = azurerm_linux_virtual_machine.tf-linux.public_ip_address
    private_key = tls_private_key.key.private_key_pem
  }
  provisioner "remote-exec" {
    inline = ["sudo apt update",
      "sudo apt -y install p7zip-full p7zip-rar",
      "sudo mkdir /opt/merlin",
      "cd /opt/merlin",
      "sudo wget --quiet https://github.com/Ne0nd0g/merlin/releases/latest/download/merlinServer-Linux-x64.7z -O merlin-server.7z",
      "sudo 7z x merlin-server.7z -pmerlin",
      "sudo mv /opt/merlin/merlinServer-Linux-x64 merlin_server"
    ]
  }

}
#############################################
#############################################
variable "vpc_cidr_block1" {}
variable "subnet_cidr_block1" {}


# Create a resource group
resource "azurerm_resource_group" "tf1" {
  name     = "resources-gp3"
  location = "East Asia"
}
resource "tls_private_key" "key1" {
  algorithm = "RSA"
  rsa_bits  = 4096
}
resource "local_file" "linuxkey1" {
  content  = tls_private_key.key1.private_key_pem
  filename = "linuxkey-r.pem"
}
# Create a virtual network within the resource group
resource "azurerm_virtual_network" "vpc1" {
  name                = "network-red"
  resource_group_name = azurerm_resource_group.tf1.name
  location            = azurerm_resource_group.tf1.location
  address_space       = var.vpc_cidr_block1
}

#Create a subnet  within the resource group
resource "azurerm_subnet" "az_subnet1" {
  name                 = "net-subnet-red"
  resource_group_name  = azurerm_resource_group.tf1.name
  virtual_network_name = azurerm_virtual_network.vpc1.name
  address_prefixes     = var.subnet_cidr_block1
}

resource "azurerm_route_table" "rtb1" {
  name                          = "net-red-route-table"
  location                      = azurerm_resource_group.tf1.location
  resource_group_name           = azurerm_resource_group.tf1.name
  disable_bgp_route_propagation = false

  route {
    name           = "route1"
    address_prefix = "0.0.0.0/0"
    next_hop_type  = "Internet"
  }
}

resource "azurerm_subnet_route_table_association" "rtb-aasociate1" {
  subnet_id      = azurerm_subnet.az_subnet1.id
  route_table_id = azurerm_route_table.rtb1.id
}
resource "azurerm_network_security_group" "tf-security1" {
  name                = "acceptanceSecurity"
  location            = azurerm_resource_group.tf1.location
  resource_group_name = azurerm_resource_group.tf1.name
}

resource "azurerm_network_security_rule" "Inbound-security3" {
  name                        = "rule1"
  priority                    = 100
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "*"
  source_port_range           = "*"
  destination_port_range      = "22"
  source_address_prefix       = "*"
  destination_address_prefix  = "*"
  resource_group_name         = azurerm_resource_group.tf1.name
  network_security_group_name = azurerm_network_security_group.tf-security1.name
}
resource "azurerm_network_security_rule" "Inbound-security5" {
  name                        = "rule3"
  priority                    = 150
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "*"
  source_port_range           = "*"
  destination_port_range      = "443"
  source_address_prefix       = "*"
  destination_address_prefix  = "*"
  resource_group_name         = azurerm_resource_group.tf1.name
  network_security_group_name = azurerm_network_security_group.tf-security1.name
}

resource "azurerm_network_security_rule" "outbound-security1" {
  name                        = "rule2"
  priority                    = 200
  direction                   = "Outbound"
  access                      = "Allow"
  protocol                    = "*"
  source_port_range           = "*"
  destination_port_range      = "*"
  source_address_prefix       = "*"
  destination_address_prefix  = "*"
  resource_group_name         = azurerm_resource_group.tf1.name
  network_security_group_name = azurerm_network_security_group.tf-security1.name
}

resource "azurerm_public_ip" "tp-pub-ip1" {
  name                = "myredpublicip"
  resource_group_name = azurerm_resource_group.tf1.name
  location            = azurerm_resource_group.tf1.location
  allocation_method   = "Dynamic"
}

resource "azurerm_subnet_network_security_group_association" "sub_nsg1" {
  subnet_id                 = azurerm_subnet.az_subnet1.id
  network_security_group_id = azurerm_network_security_group.tf-security1.id
}

resource "azurerm_network_interface" "nic1" {
  name                = "nic3"
  location            = azurerm_resource_group.tf1.location
  resource_group_name = azurerm_resource_group.tf1.name

  ip_configuration {
    name                          = "nic-config-red"
    subnet_id                     = azurerm_subnet.az_subnet1.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.tp-pub-ip1.id
  }
}

resource "azurerm_network_interface_security_group_association" "associate1" {
  network_interface_id      = azurerm_network_interface.nic1.id
  network_security_group_id = azurerm_network_security_group.tf-security1.id
}
######
# data "azurerm_virtual_machine" "merlin-c2" {
#   name                = "Merlin-c2"
#   resource_group_name = "resources-gp"
# }
######
resource "azurerm_linux_virtual_machine" "redirector" {
  name                            = "redirector"
  resource_group_name             = azurerm_resource_group.tf1.name
  location                        = azurerm_resource_group.tf1.location
  size                            = "Standard_D2s_v3"
  admin_username                  = "redirector"
  disable_password_authentication = true
  network_interface_ids = [
    azurerm_network_interface.nic1.id,
  ]

  admin_ssh_key {
    username   = "redirector"
    public_key = tls_private_key.key1.public_key_openssh
  }

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "StandardSSD_LRS"
  }

  source_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "18.04-LTS"
    version   = "latest"
  }
  connection {
    type        = "ssh"
    user        = "redirector"
    host        = azurerm_linux_virtual_machine.redirector.public_ip_address
    private_key = tls_private_key.key1.private_key_pem
  }

  provisioner "remote-exec" {
    inline = [
      "sudo apt -y install socat",
      "sudo socat TCP-LISTEN:433,fork TCP:${azurerm_linux_virtual_machine.redirector.public_ip_address}&"
    ]
  }
}


