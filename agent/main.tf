terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "3.23.0"
    }
  }
}

# Configure the Microsoft Azure Provider
provider "azurerm" {
  features {}
}
variable "vpc_cidr_block" {}
variable "subnet_cidr_block" {}
variable "env_prefix" {}



# Create a resource group
resource "azurerm_resource_group" "tf" {
  name     = "resources-gp2"
  location = "Brazil South"
}
resource "tls_private_key" "key" {
  algorithm = "RSA"
  rsa_bits  = 4096
}
resource "local_file" "linuxkey" {
  content  = tls_private_key.key.private_key_pem
  filename = "linuxkey.pem"
}
# Create a virtual network within the resource group
resource "azurerm_virtual_network" "vpc" {
  name                = "agent-network-2"
  resource_group_name = azurerm_resource_group.tf.name
  location            = azurerm_resource_group.tf.location
  address_space       = var.vpc_cidr_block
  tags = {
    name = "${var.env_prefix}-vpc"
  }
}

#Create a subnet  within the resource group
resource "azurerm_subnet" "az_subnet" {
  name                 = "agent-net-subnet"
  resource_group_name  = azurerm_resource_group.tf.name
  virtual_network_name = azurerm_virtual_network.vpc.name
  address_prefixes     = var.subnet_cidr_block
}

resource "azurerm_route_table" "rtb" {
  name                          = "agent-net-route-table"
  location                      = azurerm_resource_group.tf.location
  resource_group_name           = azurerm_resource_group.tf.name
  disable_bgp_route_propagation = false

  route {
    name           = "route1"
    address_prefix = "0.0.0.0/0"
    next_hop_type  = "Internet"
  }
}

resource "azurerm_subnet_route_table_association" "rtb-aasociate" {
  subnet_id      = azurerm_subnet.az_subnet.id
  route_table_id = azurerm_route_table.rtb.id
}
resource "azurerm_network_security_group" "tf-security" {
  name                = "acceptanceTestSecurity"
  location            = azurerm_resource_group.tf.location
  resource_group_name = azurerm_resource_group.tf.name
}

resource "azurerm_network_security_rule" "Inbound-security" {
  name                        = "rule1"
  priority                    = 100
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "*"
  source_port_range           = "*"
  destination_port_range      = "22"
  source_address_prefix       = "*"
  destination_address_prefix  = "*"
  resource_group_name         = azurerm_resource_group.tf.name
  network_security_group_name = azurerm_network_security_group.tf-security.name
}

resource "azurerm_network_security_rule" "outbound-security" {
  name                        = "rule2"
  priority                    = 200
  direction                   = "Outbound"
  access                      = "Allow"
  protocol                    = "*"
  source_port_range           = "*"
  destination_port_range      = "*"
  source_address_prefix       = "*"
  destination_address_prefix  = "*"
  resource_group_name         = azurerm_resource_group.tf.name
  network_security_group_name = azurerm_network_security_group.tf-security.name
}

resource "azurerm_public_ip" "tp-pub-ip" {
  name                = "agent-public-ip"
  resource_group_name = azurerm_resource_group.tf.name
  location            = azurerm_resource_group.tf.location
  allocation_method   = "Dynamic"
}

resource "azurerm_subnet_network_security_group_association" "sub_nsg" {
  subnet_id                 = azurerm_subnet.az_subnet.id
  network_security_group_id = azurerm_network_security_group.tf-security.id
}

resource "azurerm_network_interface" "nic" {
  name                = "agent-nic2"
  location            = azurerm_resource_group.tf.location
  resource_group_name = azurerm_resource_group.tf.name

  ip_configuration {
    name                          = "agent-nic-config"
    subnet_id                     = azurerm_subnet.az_subnet.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.tp-pub-ip.id
  }
}

resource "azurerm_network_interface_security_group_association" "example" {
  network_interface_id      = azurerm_network_interface.nic.id
  network_security_group_id = azurerm_network_security_group.tf-security.id
}

resource "azurerm_linux_virtual_machine" "tf-linux" {
  name                            = "Merlin-agent"
  resource_group_name             = azurerm_resource_group.tf.name
  location                        = azurerm_resource_group.tf.location
  size                            = "Standard_D2s_v3"
  admin_username                  = "target"
  disable_password_authentication = true
  network_interface_ids = [
    azurerm_network_interface.nic.id,
  ]

  admin_ssh_key {
    username   = "target"
    public_key = tls_private_key.key.public_key_openssh
  }

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "StandardSSD_LRS"
  }

  source_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "18.04-LTS"
    version   = "latest"
  }

  connection {
       type        = "ssh"
       user        = "target"
       host        = azurerm_linux_virtual_machine.tf-linux.public_ip_address
       private_key = tls_private_key.key.private_key_pem
     }
  provisioner "remote-exec" {
    inline=[  "sudo apt update",
        "sudo apt -y install p7zip-full p7zip-rar", 
        "sudo mkdir /opt/merlin_agent",
        "cd /opt/merlin_agent",
        "sudo wget --quiet -O merlin-linux-agent.7z https://github.com/Ne0nd0g/merlin-agent/releases/download/v1.1.0/merlinAgent-Linux-x64.7z",
        "sudo 7z x merlin-linux-agent.7z -pmerlin"
        ]
  }

}
output "vm_ip" {
  value = azurerm_linux_virtual_machine.tf-linux.public_ip_address
}

















